<?php
    header('Content-Type: application/json');
    $message = file_get_contents('php://input');
    if($message->is_null())
        http_response_code(405);
    else if(empty(json_decode($message)))
        http_response_code(400);
    $output = array("text" => base64_decode(json_decode($message)->{"to-translate"}));
    echo json_encode($output);
?>
<?php 
    header('Content-Type: application/json');
    $input = file_get_contents("php://input");
    if($input->is_null())
        http_response_code(405);
    else if(empty(json_decode($input)))
        http_response_code(400);
    $output = array("text" => deleteDoubledChars($input->{"to-translate"}));
    echo json_encode($output);

    function deleteDoubledChars($str)
    {
        $i = 0;
        $to_send = "";
        while ($i < strlen($str))
        {
            if($i == strlen($str) - 1 || $str[$i] != $str[$i + 1])
                $to_send .= $str[$i];
            else
                $i += 1;
            $i += 1;
        }
        return $to_send;
    }
?>
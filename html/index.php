<?php
    $url = $_SERVER["REQUEST_URI"];
    $exo = "nightcode";
    //echo $url . "<br/>";
    if (strpos($url, '/api/greetings') !== false) {
        $exo = 'hello';
    }
    switch ($exo) {
        case "hello": {
            $message = array('message' => 'hello, ');
            header('Content-Type: application/json');
            if (isset($_GET['name'])) {
                if (empty($_GET['name'])) {
                    http_response_code(400);
                }
            }
            if (isset($_GET['name'])) {
                $message['message'] .= $_GET['name'];
            }
            else{

                $json = json_decode(file_get_contents('php://input'));
                if(!empty($json))
                {
                    if(empty($json->{"name"}))
                        http_response_code(400);
                    $message['message'] .= $json->{"name"};
                }
                else {
                    $message['message'] .= "world";
                }
            }
            echo json_encode($message);
            break;
        }

        case "nightcode": {
            echo "NightCode";
            break;
        }
    }
?>
